import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sql_qr_app/src/models/scan_model.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._private();

  DBProvider._private();

  Future<Database> get database async {
    if(_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'ScansDB.db');
    return await openDatabase(
      path,
      version: 1,
      onOpen: (Database db) {},
      onCreate: (Database db, int version) async {
        await db.execute(
          'CREATE TABLE Scans ('
          ' id INTEGER PRIMARY KEY,'
          ' type TEXT,'
          ' value TEXT)',
        );
      },
    );
  }

  //CREAR REGISTROS
  Future<int> insertScan(ScanModel model) async {
    final db = await database;
    final result = await db.rawInsert(
      'INSERT INTO Scans (id, type, value) '
      ' VALUES (${model.id}, "${model.type}", "${model.value}")'
    );
    return result;
  }

  Future<int> insert(String table, Map<String, dynamic> jsonModel) async {
    final db = await database;
    return db.insert(table, jsonModel);
  }

  Future<ScanModel> getOne(int id) async {
    final db = await database;
    final result = await db.query('Scans', where: 'id = ?', whereArgs: [id]);
    return result.isNotEmpty ? ScanModel.fromJson(result.first) : null;
  }

  Future<List<ScanModel>> getAll() async {
    final db = await database;
    final result = await db.query('Scans');
    return result.isNotEmpty ? result.map((r) => ScanModel.fromJson(r) ).toList() : [];
  }

  Future<ScanModel> getByType(String type) async {
    final db = await database;
    final result = await db.query('Scans', where: 'type = ?', whereArgs: [type]);
    return result.isNotEmpty ? ScanModel.fromJson(result.first) : null;
  }

  Future<int> updateOne(ScanModel model) async {
    final db = await database;
    final result = await db.update('Scans', model.toJson(), where: 'id = ?', whereArgs: [model.id]);
    return result;
  }

  Future<int> deleteOne(int id) async {
    final db = await database;
    final result = await db.delete('Scans', where: 'id = ?', whereArgs: [id]);
    return result;
  }

  Future<int> deleteAll() async {
    final db = await database;
    final result = await db.delete('Scans');
    return result;
  }

}