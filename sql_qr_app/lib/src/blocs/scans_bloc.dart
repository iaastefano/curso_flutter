import 'dart:async';

import 'package:sql_qr_app/src/providers/db_provider.dart';

import '../models/scan_model.dart';


class ScansBloc {
  static final ScansBloc _singleton = new ScansBloc._private();

  factory ScansBloc() {
    return _singleton;
  }

  ScansBloc._private() {
    //Obtener los scans de la base de datos
    obtenerScans();
  }

  final _scansStreamController = StreamController<List<ScanModel>>.broadcast();

  Stream<List<ScanModel>> get scansStream => _scansStreamController.stream;

  dispose() {
    _scansStreamController?.close();
  }

  obtenerScans() async {
    _scansStreamController.sink.add(await DBProvider.db.getAll());
  }

  borrarScan(int id) async {
    await DBProvider.db.deleteOne(id);
    obtenerScans();
  }

  borrarScans() async {
    await DBProvider.db.deleteAll();
    obtenerScans();
    // _scansStreamController.sink.add([]);
  }

  agregarScan(ScanModel model) async {
    await DBProvider.db.insertScan(model);
    obtenerScans();
  }

}