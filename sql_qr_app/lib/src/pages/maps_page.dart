import 'package:flutter/material.dart';
import 'package:sql_qr_app/src/blocs/scans_bloc.dart';
import 'package:sql_qr_app/src/models/scan_model.dart';
import 'package:sql_qr_app/src/utils/scan_util.dart' as scanUtils;

class MapsPage extends StatelessWidget {

  final scansBloc = new ScansBloc();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child:
        StreamBuilder(
          stream: scansBloc.scansStream,
          builder: (BuildContext context, AsyncSnapshot<List<ScanModel>> snapshot) {
            if(!snapshot.hasData) {
              return Center(child: CircularProgressIndicator(),); 
            }
            final scans = snapshot.data;
            if(scans.isEmpty) {
              return Center(child: Text('No hay información'));
            }
            return ListView.builder(
              itemCount: scans.length,
              itemBuilder: (BuildContext context, int i) => Dismissible(
                key: UniqueKey(),
                background: Container(color: Colors.red,),
                onDismissed: (direction) => scansBloc.borrarScan(scans[i].id),
                child: ListTile(
                  title: Text(scans[i].value),
                  subtitle:  Text('ID: ' + scans[i].id.toString()),
                  leading: Icon(Icons.map),
                  trailing: Icon(Icons.keyboard_arrow_right),
                  onTap: () => scanUtils.abrirScan(context, scans[i]),
                ),
              )
            );
          },
        ),
      ),
    );
  }
}