import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:sql_qr_app/src/models/scan_model.dart';

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ScanModel scan = ModalRoute.of(context).settings.arguments;

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Coordenadas QR'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.my_location),
              onPressed: () {},
            )
          ],
        ),
        body: _crearMap(scan),
      ),
    );
  }

  Widget _crearMap(ScanModel scan) {
    return new FlutterMap(
      options: new MapOptions(
        center: scan.getLatLng(),
        zoom: 13.0,
      ),
      layers: [
        _crearMapa(),
        new MarkerLayerOptions(
          markers: [
            new Marker(
              width: 80.0,
              height: 80.0,
              point: scan.getLatLng(),
              builder: (ctx) => new Container(
                child: new FlutterLogo(),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _crearMapa() {
    return new TileLayerOptions(
      urlTemplate: "https://api.mapbox.com/v4/"
          "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
      additionalOptions: {
        'accessToken':
            'pk.eyJ1IjoiaWFhc3RlZmFubyIsImEiOiJjazFsZXo4czgwNGYwM2RwY3l2YWZzN2gxIn0.uWbb_XLeNRVALPTWBfcJ2w',
        'id': 'mapbox.streets',
      },
    );
  }
}
