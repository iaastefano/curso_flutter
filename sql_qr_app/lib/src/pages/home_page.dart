import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:sql_qr_app/src/blocs/scans_bloc.dart';
import 'package:sql_qr_app/src/models/scan_model.dart';
import 'package:sql_qr_app/src/pages/maps_page.dart';
import 'package:sql_qr_app/src/utils/scan_util.dart' as scanUtils;
import 'directions_page.dart';

enum Pages {
  maps,
  directions,
} 

class HomePage extends StatefulWidget {


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Pages currentPage = Pages.maps;

  ScansBloc scansBloc = new ScansBloc();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title:  Text('QR SQL'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.delete_forever),
              onPressed: scansBloc.borrarScans,
            )
          ],
        ),
        body: _callPage(currentPage),
        bottomNavigationBar: _bottomNavigationBar(),
        floatingActionButton: _floatingActionBottom(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }

  Widget _bottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: 0,
      onTap: (index) {
        setState(() {
          currentPage = Pages.values[index];
        });
      },
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.map),
          title: Text('Mapas'), 
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.directions),
          title: Text('Direcciones'), 
        ),
      ],
    );
  }

  Widget _callPage(Pages actualPage) {
    switch(actualPage) {
      case Pages.maps: return MapsPage();
      case Pages.directions: return DirectionsPage();
      default: return HomePage();
    }
  }

  _floatingActionBottom() {
    return FloatingActionButton(
      child: Icon(Icons.filter_center_focus),
      onPressed: () {
        _scanQr();
      },
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  Future _scanQr() async {
    String futureString = 'https://fernando-herrera.com';
    try {
      // futureString = await new QRCodeReader().scan();
      if (futureString != null) {
        final scan = ScanModel(value: futureString);
        final scan2 = ScanModel(value: 'geo:40.724233047051705,-74.00731459101564');
        final scansBloc = new ScansBloc();
        scansBloc.agregarScan(scan);
        scansBloc.agregarScan(scan2);
        // if(Platform.isIOS) {
        //   Future.delayed(Duration(milliseconds: 750), () {
        //     scanUtils.abrirScan(context, scan);
        //   });
        // } else {
        //   scanUtils.abrirScan(context, scan);
        // }
      }
    } catch (e) {
      print(e);
    }
  }
}