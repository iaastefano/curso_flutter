

import 'package:flutter/cupertino.dart';
import 'package:sql_qr_app/src/models/scan_model.dart';
import 'package:url_launcher/url_launcher.dart';

abrirScan(BuildContext context, ScanModel model) async {
  if(model.type == 'http') {
    if (await canLaunch(model.value)) {
      await launch(model.value);
    } else {
      throw 'Could not launch ${model.value}';
    }
  } else {
    Navigator.pushNamed(context, 'map', arguments: model);
  }
}