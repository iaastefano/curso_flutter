import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class ButtonsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          _titulos(),
        ],
      ),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }

  Widget _bottomNavigationBar(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
        primaryColor: Colors.pinkAccent,
        textTheme: Theme.of(context).textTheme.copyWith(
          caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0))
        )
      ),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.access_alarm),
            title: Container()
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.accessible),
            title: Container()
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            title: Container()
          ),
        ],
      ),
    );
  }

  Widget _fondoApp() {

    final gradiente = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset(0.0, 0.6),
          end:   FractionalOffset(0.0, 1.0),
          colors: [
            Color.fromRGBO(52, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0),            
          ]
        )
      ),
    );

    final _container = Container(
      height: 320.0,
      width: 320.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(70),
        gradient: LinearGradient(
          begin: FractionalOffset(0.0, 0.6),
          end:   FractionalOffset(0.0, 1.0),
          colors: [
            Color.fromRGBO(236, 98, 188, 1.0),
            Color.fromRGBO(241, 142, 172, 1.0),            
          ]
        )
      ),
    );

    final cajaRosa = Transform.rotate(
      angle: - pi / 4.2,
      child: _container,
    );

    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(
          top: -90.0,
          child: cajaRosa,
        ),
      ],
    );
  }

  Widget _titulos() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Classify transaction', style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),),
            SizedBox(height: 10,),
            Text('Classify this transaction into a particular category', style: TextStyle(color: Colors.white, fontSize: 18),),
            _botonesRedondeados(),
          ],
        ),
      ),
    );
  }

  Widget _botonesRedondeados() {
    return Table(
      children: <TableRow>[
        TableRow(
          children: [
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),            
          ]
        ),
        TableRow(
          children: [
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),            
          ]
        ),
        TableRow(
          children: [
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),
            _botonRedondeado(Colors.pinkAccent, Icons.account_balance, 'Account'),            
          ]
        ),
      ],
    );
  }

  _botonRedondeado(Color color, IconData icon, String text) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 2, sigmaY: 2
        ),
        child: Container(
          height: 140,
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Color.fromRGBO(62, 66, 107, 0.7),
            borderRadius: BorderRadius.circular(15)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(height: 10,),
              CircleAvatar(
                backgroundColor: color,
                radius: 30,
                child: Icon(icon, color: Colors.white, size: 25,),
              ),
              Text(text, style: TextStyle(color: color),)
            ],
          ),
        ),
      ),
    );
  }
}