
import 'package:flutter/material.dart';

class BasicPage extends StatelessWidget {
  final _titleStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
  final _subtitleStyle = TextStyle(fontSize: 14, color: Colors.grey);  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _crearImagen(),
                _crearTitulo(),
                _crearBotones(),
                _crearTexto(),
              ],
            )
          ),
        )
      ),
    );
  }

  Widget _crearImagen() {
    return Container(
              child: Image(
                image: NetworkImage('https://static.photocdn.pt/images/articles/2018/03/09/articles/2017_8/landscape_photography.jpg'),
                fit: BoxFit.cover,
              ),
            );
  }

  Widget _crearTitulo() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Rio y montañas', style: _titleStyle,),
                SizedBox(height: 8.0,),
                Text('Un río entre montañas con sol', style: _subtitleStyle, overflow: TextOverflow.ellipsis,),
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Icon(Icons.star, color: Colors.red,),
                Text('41'),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _crearBotones() {
    return Container(
      // padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _crearBoton('CALL', Icons.call),
          _crearBoton('ROUTE', Icons.near_me),
          _crearBoton('SHARE', Icons.share),
        ],
      ),
    );
  }

  Widget _crearBoton(String title, IconData icon) {
     
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(icon, color: Colors.blue,),
          onPressed: () {},
        ),
        Text(title, style: TextStyle(color: Colors.blue, fontSize: 15),)
      ],
    );
  }

  Widget _crearTexto() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Text('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,'),
    );
  }
}