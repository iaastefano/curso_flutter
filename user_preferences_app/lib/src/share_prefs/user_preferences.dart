import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instance = new UserPreferences._private();

  factory UserPreferences() {
    return _instance;
  }

  UserPreferences._private();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // ninguna de estas propiedades se usa
  // bool _colorSecundario;
  // int genero;
  // String nombre;

  //GET y SET del genero
  get genero {
    return _prefs.getInt('genero') ?? 1;
  }
  set genero (int value) {
    _prefs.setInt('genero', value);
  }
  
  //GET y SET del colorSecundario
  get colorSecundario {
    return _prefs.getBool('colorSecundario') ?? false;
  }
  set colorSecundario (bool value) {
    _prefs.setBool('colorSecundario', value);
  }

  //GET y SET del nombre
  get nombre {
    return _prefs.getString('nombre') ?? 'Invitado';
  }
  set nombre (String value) {
    _prefs.setString('nombre', value);
  }

    //GET y SET del ultimaPagina
  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'home';
  }
  set ultimaPagina (String value) {
    _prefs.setString('ultimaPagina', value);
  }
}