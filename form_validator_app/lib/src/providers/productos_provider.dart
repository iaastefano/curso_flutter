import 'dart:convert';
import 'dart:io';

import 'package:form_validator_app/src/preferences/user_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:form_validator_app/src/models/producto_model.dart';
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';

class ProductosProvider {
  
  final String _url = "https://my-flutter-projects.firebaseio.com";
  final _prefs = new UserPreferences();

  Future<bool> crearProducto(ProductoModel producto) async {
    final url = '$_url/productos.json?auth=${_prefs.token}';
    final response = await http.post(url, body: productoModelToJson(producto));
    final decodedData = json.decode(response.body);
    return true;
  }

  Future<List<ProductoModel>> cargarProductos() async {
    final url = '$_url/productos.json?auth=${_prefs.token}';
    final response = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(response.body);
    
    print(decodedData);

    if(decodedData == null) return [];
    
    if (decodedData['error'] != null ) return [];

    List<ProductoModel> productos = [];

    decodedData.forEach((id, prod) {
      final prodTemp = ProductoModel.fromJson(prod);
      prodTemp.id = id;
      productos.add(prodTemp);
    });

    return productos;
  }

  Future<bool> borrarProducto(String id) async {
    final url = '$_url/productos/$id.json?auth=${_prefs.token}';
    final response = await http.delete(url);
    return true;
  }

  Future<bool> editarProducto(ProductoModel producto) async {
    final url = '$_url/productos/${producto.id}.json?auth=${_prefs.token}';
    final response = await http.put(url, body: productoModelToJson(producto));
    final decodedData = json.decode(response.body);
    return true;
  }

  Future<String> subirImagen(File imagen) async {
    final apiKey = 'gc6riygg';
    
    final url = Uri.parse('https://api.cloudinary.com/v1_1/iaastefano/image/upload?upload_preset=$apiKey');
    
    final mimeType = mime(imagen.path).split('/'); //image/jpeg => [0] = image & [1] = jpeg
    
    final imageUploadRequest = http.MultipartRequest(
      'POST',
      url
    );
    
    final file = await http.MultipartFile.fromPath(
      'file',
      imagen.path,
      contentType: MediaType(mimeType[0], mimeType[1]),
    );
    
    imageUploadRequest.files.add(file);
    
    final streamResponse = await imageUploadRequest.send();
    final response = await http.Response.fromStream(streamResponse);
    
    if(response.statusCode != 200 && response.statusCode != 201) {
      return null;
    }

    final respData = json.decode(response.body);
    
    return respData['secure_url'];
  }

}