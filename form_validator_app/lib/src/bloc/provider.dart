import 'package:flutter/material.dart';
import 'package:form_validator_app/src/bloc/login_bloc.dart';
import 'package:form_validator_app/src/bloc/productos_bloc.dart';

class Provider extends InheritedWidget {

  final loginBloc      = LoginBloc();
  final _productosBloc = ProductosBloc();

  static Provider _instancia;

  factory Provider({Key key, Widget child}) {
    if(_instancia == null) {
      _instancia = new Provider._internal(key: key, child: child);
    }
    return _instancia;
  }
  

  Provider._internal({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
  
  static LoginBloc of(BuildContext context) {
    //toma el contexto y va a buscar un widget exactamente con el tipo "Provider"
    return (context.inheritFromWidgetOfExactType(Provider) as Provider).loginBloc;
  }

  static ProductosBloc productosBloc(BuildContext context) {
    //toma el contexto y va a buscar un widget exactamente con el tipo "Provider"
    return (context.inheritFromWidgetOfExactType(Provider) as Provider)._productosBloc;
  }
}