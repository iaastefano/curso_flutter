import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedConteinerPage extends StatefulWidget {
  _AnimatedConteinerPageState createState() => _AnimatedConteinerPageState();
}

class _AnimatedConteinerPageState extends State<AnimatedConteinerPage> {
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.pink;

  BorderRadius _borderRadius = BorderRadius.circular(8.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color
          ), 
          duration: Duration(milliseconds: 500),
          curve: Curves.bounceInOut,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () {
          final random = Random();
          setState(() {
            _width = random.nextInt(200).toDouble();
            _height = random.nextInt(200).toDouble();
            _color = Color.fromRGBO(
              random.nextInt(255),
              random.nextInt(255),
              random.nextInt(255),
              1
              );
            _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble());
          });
        },
      ),
    );
  }
}