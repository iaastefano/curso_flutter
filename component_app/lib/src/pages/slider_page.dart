import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  SliderPage({Key key}) : super(key: key);

  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _sliderValue = 0;
  bool _blockSlider = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('Slider Page'),
       ),
       body: Container(
         padding: EdgeInsets.only(top: 50),
         child: Column(
           children: <Widget>[
             _crearSlider(),
             _crearCheckBox(),
             _crearSwitch(),
             _crearImagen(),
           ],
         )
       ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      label: 'Tamaño de la imagen',
      activeColor: Colors.pink,
      onChanged:(_blockSlider) ? null :
      (double value) {
        setState(() {
          _sliderValue = value; 
        });
      },
      value: _sliderValue,
      min: 0,
      max: 500,
    );
  }

  _crearImagen() {
    return Expanded(
        child: Image(
        image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/c/c9/Boca_escudo.png'),
        width: _sliderValue,
        fit: BoxFit.contain,
      ),
    );
  }

  Widget _crearCheckBox() {
    // return Checkbox(
    //   value: _blockSlider,
    //   onChanged: (value) {
    //     setState(() {
    //       _blockSlider = value;
    //     });
    //   },
    // );
    return CheckboxListTile(
      title: Text('Bloquear Slider: '),
      value: _blockSlider,
      onChanged: (value) {
        setState(() {
          _blockSlider = value;
        });
      },
    );
  }

  Widget _crearSwitch() {
    return SwitchListTile(
      title: Text('Bloquear Slider: '),
      value: _blockSlider,
      onChanged: (value) {
        setState(() {
          _blockSlider = value;
        });
      },
    );
  }
}