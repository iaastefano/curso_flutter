import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas_app/models/movie_model.dart';

class CardSwiper extends StatelessWidget {

  final List<Movie> peliculas;

  CardSwiper({@required this.peliculas});

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;

    return Container(
      // padding: EdgeInsets.only(top:10.0),
      child: Swiper(
        itemCount: peliculas.length,
        itemWidth: _screenSize.width * 0.5,
        itemHeight: _screenSize.height * 0.5,
        layout: SwiperLayout.STACK,  
        itemBuilder: (BuildContext context,int index){
          Movie pelicula = peliculas[index];
          pelicula.uniqueId = '${pelicula.id}-card';
          return Container(
            child: Hero(
              tag: pelicula.uniqueId,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: GestureDetector(
                  child: FadeInImage(
                    fit: BoxFit.cover,
                    image: _loadImage(pelicula),
                    placeholder: AssetImage('assets/img/no-image.jpg'),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "movie", arguments: pelicula);
                  },
                ),
              ),
            ),
          );
        },
        // pagination: new SwiperPagination(),
        // control: new SwiperControl(),
      ),
    );

  }

  ImageProvider _loadImage(Movie pelicula) {
    final poster = pelicula.getPosterImage();
    if(poster == null)
      return AssetImage('assets/img/no-image.jpg');
    else
      return NetworkImage(poster);
  }


}