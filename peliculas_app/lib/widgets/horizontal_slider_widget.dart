
import 'package:flutter/material.dart';
import 'package:peliculas_app/models/movie_model.dart';

class HorizontalSlider extends StatelessWidget {

  final List<Movie> peliculas;
 
  final Function siguientePagina;

  final PageController _pageController = new PageController(
    initialPage: 1,
    viewportFraction: 0.24
  );

  HorizontalSlider({@required this.peliculas, @required this.siguientePagina});

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if(_pageController.position.pixels >= _pageController.position.maxScrollExtent - 200)
      {
        siguientePagina();
      }
    });

    return Container(
      // padding: EdgeInsets.only(top:10.0),
      height: _screenSize.height * 0.25,
      // child: PageView(children: _tarjetas()),
      child: PageView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: peliculas.length,
        pageSnapping: false,
        controller: _pageController, 
        itemBuilder: (BuildContext context, int index){
            return _tarjeta(peliculas[index], context);
        },
      ),
    );
  }

  ImageProvider _loadImage(Movie movie) {
    final poster = movie.getPosterImage();
    if(poster == null)
      return AssetImage('assets/img/no-image.jpg');
    else
      return NetworkImage(poster);
  }

  Widget _tarjeta(Movie pelicula, BuildContext context) {
    pelicula.uniqueId = '${pelicula.id}-horizontal';
    final tarjeta = Container(
      // margin: EdgeInsets.only(right: 0.0),
      child: Column(
        children: <Widget>[
          Hero(
            tag: pelicula.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.5),
              child: FadeInImage(
                fit: BoxFit.cover,
                image: _loadImage(pelicula),
                placeholder: AssetImage('assets/img/no-image.jpg'),
                height: 100.0
              ),
            ),
          ),
          SizedBox(height: 5.0,),
          Text(
            pelicula.title,
            style: Theme.of(context).textTheme.subtitle,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );

    return GestureDetector(
      child: tarjeta,
      onTap: () {
        Navigator.pushNamed(context, "movie", arguments: pelicula);
      },
    );
  }

  // List<Widget> _tarjetas() {
  //   return peliculas.map((p) {
  //     return _tarjeta(p); 
  //   }).toList();
  // }


}