import 'package:flutter/material.dart';
import 'package:peliculas_app/pages/home_page.dart';
import 'package:peliculas_app/pages/movie_detail.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Peliculas',
      initialRoute: '/',
      routes: {
        '/' : (BuildContext context) => HomePage(),
        'movie' : (BuildContext context) => MovieDetail(),
      },
    );
  }
}