import 'package:flutter/material.dart';
import 'package:peliculas_app/providers/movies_provider.dart';
import 'package:peliculas_app/search/search_delegate.dart';
import 'package:peliculas_app/widgets/card_swiper_widget.dart';
import 'package:peliculas_app/widgets/horizontal_slider_widget.dart';

class HomePage extends StatelessWidget {

  final moviesProvider = new MoviesProvider();

  @override
  Widget build(BuildContext context) {
    moviesProvider.getPopulars();
    return Scaffold(
      appBar: AppBar(
        title: Text('Peliculas en Cines'),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              // showSearch(
              //   context: context,
              //   delegate: SearchDelegate(

              //     keyboardType: TextInputType(TextInputType.text),

              //   )
              // );
              showSearch(context: context, delegate: DataSearch());
            },
            ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _swipeCards(),
          _footer(context)
        ],
      ),
    );
  }

  Widget _swipeCards() {
    return FutureBuilder(
      future: moviesProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        if(snapshot.hasData) {
          return CardSwiper(peliculas: snapshot.data);
        } 
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator()
            ),
          );
        }
      },
    );
  }

  Widget _footer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0),
            child: Text('Populares', style: Theme.of(context).textTheme.subtitle, textAlign: TextAlign.left,)
          ),
          SizedBox(height: 10.0,),
          StreamBuilder(
            stream: moviesProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
              if(snapshot.hasData) {
                return HorizontalSlider(
                  peliculas: snapshot.data,
                  siguientePagina: moviesProvider.getPopulars,
                );
              }
              else {
                return Container(
                  child: Center(
                    child: CircularProgressIndicator()
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }
}