import 'package:flutter/material.dart';
import 'package:peliculas_app/models/movie_model.dart';
import 'package:peliculas_app/providers/movies_provider.dart';

class DataSearch extends SearchDelegate {

  final moviesProvider = new MoviesProvider();

  String seleccion = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    // las acciones de nuestro appbar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () => query = '',
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // icono a la izquierda del appbar
        return IconButton(
          icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            progress: transitionAnimation,
          ),
          onPressed: () => close(context, null),
        );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    
    if(query.isEmpty) return Container();

    return FutureBuilder(
      future: moviesProvider.buscarPeliculas(query),
      builder: (BuildContext context, AsyncSnapshot<List<Movie>> snapshot) {
        if(snapshot.hasData) {
          final movies = snapshot.data;
          return ListView(
            children: movies.map((m) {
              return ListTile(
                leading: FadeInImage(
                  image: NetworkImage(m.getPosterImage()),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  width: 50.0,
                  fit: BoxFit.contain,
                  fadeInDuration: Duration(milliseconds: 100),
                ),
                title: Text(m.title),
                subtitle: Text(m.originalTitle),
                onTap: () {
                  close(context, null);
                  m.uniqueId = '';
                  Navigator.pushNamed(context, 'movie', arguments: m);
                },
              );
            }).toList(),
          );
        }
        else {
          return Center(child: CircularProgressIndicator(),);          
        }
      },
    );

  }

}