import 'dart:async';
import 'dart:convert';

import 'package:peliculas_app/models/actor_model.dart';
import 'package:peliculas_app/models/movie_model.dart';
import 'package:http/http.dart' as http;

class MoviesProvider {
  String _apiKey  = '2dc322487542bc7d59a369f9963b8c43';
  String _url      = 'api.themoviedb.org';
  String _language = 'es-ES';

  int _popularesPage = 0;
  bool _cargando     = false;

  List<Movie> _populares = List();

  //definicion del stream

  final _popularesStreamController = StreamController<List<Movie>>.broadcast();

  Function(List<Movie>) get popularesSink => _popularesStreamController.sink.add;

  Stream<List<Movie>> get popularesStream => _popularesStreamController.stream;

  void disposeStreams() {
    _popularesStreamController?.close();
  }

  //fin definicion del stream

  Future<List<Movie>> getEnCines() async {

    final unencodedPath = '3/movie/now_playing';
    final url = Uri.https(_url, unencodedPath, {
      'api_key' : _apiKey,
      'language': _language,
    });
    
    return _processResponse(url);
  }

  Future<List<Movie>> getPopulars() async {
    if( _cargando ) return [];
    _cargando = true;
    _popularesPage++;
    final unencodedPath = '3/movie/popular';
    final url = Uri.https(_url, unencodedPath, {
      'api_key' : _apiKey,
      'language': _language,
      'page': _popularesPage.toString(),
    });
    final response = await _processResponse(url);
    _populares.addAll(response);
    popularesSink(_populares);
    _cargando = false;
    return response;
  }

  Future<List<Actor>> getCast(String movieId) async {
    final unencodedPath = '3/movie/$movieId/credits';
    final url = Uri.https(_url, unencodedPath, {
      'api_key' : _apiKey,
      'language': _language,
      // 'page': _popularesPage.toString(),
    });
    final response = await _processResponseActors(url);
    return response;
  }

  Future<List<Movie>> _processResponse(Uri url) async {
    final jsonResponse = await http.get(url.toString());
    final decodedData = json.decode(jsonResponse.body);
    
    final movies = Movies.fromJsonList(decodedData['results']);
    
    return movies.items;
  }

  Future<List<Actor>> _processResponseActors(Uri url) async {
    final jsonResponse = await http.get(url.toString());
    final decodedData = json.decode(jsonResponse.body);
    
    final cast = Cast.fromJsonListMap(decodedData['cast']);
    
    return cast.actors;
  }

  Future<List<Movie>> buscarPeliculas(String query) async {
    final unencodedPath = '3/search/movie';
    final url = Uri.https(_url, unencodedPath, {
      'api_key' : _apiKey,
      'language': _language,
      'query'   : query,
    });
    return await _processResponse(url);
  }

  
}